import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { WorkRecordComponent } from './work-record/work-record.component';
import { HomeComponent } from './home/home.component';

const routes: Routes = [
  { path:'', component: HomeComponent },
  { path: 'work-record', component: WorkRecordComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
