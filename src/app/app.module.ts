import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';
import { ScatterComponent } from './scatter/scatter.component';
import { LeftColumnComponent } from './left-column/left-column.component';
import { SecondScatterComponent } from './second-scatter/second-scatter.component';
import { DocumentDetailComponent } from './document-detail/document-detail.component';
import { EventDetailComponent } from './event-detail/event-detail.component';
import { WorkRecordComponent } from './work-record/work-record.component';
import { HomeComponent } from './home/home.component';


import { AppRoutingModule } from './app-routing.module';


@NgModule({
  declarations: [
    AppComponent,
    ScatterComponent,
    LeftColumnComponent,
    SecondScatterComponent,
    DocumentDetailComponent,
    EventDetailComponent,
    WorkRecordComponent,
    HomeComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }
