import { Component, OnInit, Input} from '@angular/core';
import { data } from '../../environments/environment';

@Component({
  selector: 'app-work-record',
  templateUrl: './work-record.component.html',
  styleUrls: ['./work-record.component.css']
})
export class WorkRecordComponent implements OnInit {

  data_selected = data[0]
  constructor(){ }

  ngOnInit(): void {
    if (history.state.id == 0) {
      this.data_selected = data[0]
    }
    if (history.state.id == 1) {
      this.data_selected = data[1]
    }
  }
}
